import { Meteor } from 'meteor/meteor';
import '../imports/startup/server/index'
import {Todos} from "../imports/api/todos/collections";
import {Random} from 'meteor/random'
Meteor.startup(() => {
  // code to run on server at startup
    if(!Todos.find().count()) Todos.insert({_id: Random.id(),text:'todo1',checked: false})
});
