import './todos.html'
import {Todos} from "../api/todos/collections";


Template.todos.onCreated(function (){
    this.autorun(()=>{
        this.subscribe('getTodos')
    })
})

Template.todos.helpers({
    allTodos(){
        return Todos.find({})
    }
})

Template.todos.events({
    'submit #newTodo'(e, temp) {
        e.preventDefault();
        let target = e.target;
        let todo = target.todo.value;
        let data = {
            text: todo,
            checked: false
        }
        Meteor.call('addTodo',data,(err,res)=>{
            if(err) console.log(err)
            if(res) console.log(res)
        })
    },
    'change .checkTodo'(e, temp) {
       let _id = this._id;
       let todo=Todos.findOne({_id})
        Meteor.call('checkTodo',_id,!todo.checked,(err,res)=>{
            if(err) console.log(err)
            if(res) console.log(res)
        })
    }
})
