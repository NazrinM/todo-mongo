import {Todos} from "./collections";

Meteor.methods({
    addTodo(data){
        return Todos.insert(data)
    },
    removeTodo(_id){
        return Todos.remove({_id:_id})
    },
    updateTodo(_id,data){
        data={checked: false}
        return Todos.update({_id},{$set:data})
    },
    checkTodo(_id,checked){
        return Todos.update({_id},{$set:{checked}})
    }
})
